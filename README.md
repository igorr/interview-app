# InterviewApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.6.

## Development server

Run `npm run json-server` for a mocked data server.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Refactoring goals

* Split view and app logic using components and services
* Add unit tests
* Add TS types
