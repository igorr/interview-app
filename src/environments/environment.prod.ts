import { AppConfig } from './../app/shared/models/app-config';

export const environment = {
  production: true,
  config: {
    apiUrl: 'http://localhost:3000',
  } as AppConfig
};
