import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersListComponent } from './users-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MatTableModule, MatButtonModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { UsersService } from './../../shared/services/users.service';
import { USERS } from './../../shared/test/users.mock';
import { of } from 'rxjs';

describe('UsersListComponent', () => {
  let component: UsersListComponent;
  let fixture: ComponentFixture<UsersListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        UsersListComponent,
      ],
      imports: [
        RouterTestingModule,
        MatTableModule,
        HttpClientModule,
        MatButtonModule,
      ],
      providers: [
        {provide: UsersService, useValue: {
          getUsers: () => of({entities: USERS}),
        }},
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should pass users to MatTableDataSource', () => {
    expect(component.usersData.data).toEqual(USERS);
  });
});
