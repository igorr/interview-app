import { Component, OnInit } from '@angular/core';
import { UsersService } from './../../shared/services/users.service';
import { User } from './../../shared/models/user';
import { MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs';
import { Dictionary } from '@ngrx/entity';

@Component({
  selector: 'int-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  private users$: Observable<Dictionary<User>>;
  displayedColumns: string[] = ['name', 'phone', 'city', 'id'];
  usersData = new MatTableDataSource<User>();

  constructor(
    private usersService: UsersService,
  ) {
    this.users$ = this.usersService.getUsers();
  }

  ngOnInit() {
    this.users$.subscribe(user => {
      this.usersData.data = user && Object.values(user.entities);
    });
  }

}
