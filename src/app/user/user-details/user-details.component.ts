import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from './../../shared/models/user';
import { FormBuilder, Validators } from '@angular/forms';
import { UsersService } from './../../shared/services/users.service';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'int-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {

  id: number;
  user: User;
  form: any;
  progress = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private usersService: UsersService,
  ) {
    this.route.params.subscribe(params => {
      this.id = params.id;
    });
  }

  ngOnInit() {
    this.usersService.getUser(this.id).pipe(
      takeWhile(() => !this.user)
    ).subscribe(user => {
      if (!user) {
        return;
      }
      this.user = user;
      this.form = this.fb.group(this.getFormData(user));
    });
  }

  getFormData(user: User) {
    return Object.entries(user).reduce((formData, [key, value]) => {
      return { ...formData, [key]: [value, [Validators.required]] };
    }, {});
  }

  saveUser(): void {
    this.progress = true;
    this.usersService.updateUser(this.form.getRawValue()).subscribe(() => {
      this.progress = false;
    })
  }

  deleteUser(): void {
    this.progress = true;
    this.usersService.deleteUser(this.form.getRawValue()).subscribe(() => {
      this.progress = false;
      this.router.navigate(['/users']);
    })
  }

}
