import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDetailsComponent } from './user-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule, MatInputModule } from '@angular/material';
import { UsersService } from '../../shared/services/users.service';
import { of } from 'rxjs';
import { USERS } from './../../shared/test/users.mock';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('UserDetailsComponent', () => {
  let component: UserDetailsComponent;
  let fixture: ComponentFixture<UserDetailsComponent>;
  let usersService: UsersService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        UserDetailsComponent,
      ],
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
        HttpClientModule,
        MatInputModule,
        MatButtonModule,
        BrowserAnimationsModule,
        NoopAnimationsModule,
      ],
      providers: [
        {provide: UsersService, useValue: {
          getUser: () => of(USERS[0]),
          updateUser: () => of(USERS[0]),
          deleteUser: () => of(null),
        }},
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    usersService = TestBed.get(UsersService);
    fixture = TestBed.createComponent(UserDetailsComponent);
    component = fixture.componentInstance;
    component.id = USERS[0].id;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get user', () => {
    expect(component.user).toEqual(USERS[0]);
  });

  it('should build form', () => {
    expect(component.form).toBeTruthy();
  });
});
