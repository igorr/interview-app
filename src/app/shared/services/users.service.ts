import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Observable } from 'rxjs';
import { User } from './../models/user';
import { select, Store } from '@ngrx/store';
import * as actions from '../entities/user.actions';
import * as fromUser from '../entities/user.reducer';
import { Dictionary } from '@ngrx/entity';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  apiUrl = environment.config.apiUrl;

  constructor(
    private httpClient: HttpClient,
    private store: Store<fromUser.State>,
  ) {
    this.loadUsers().subscribe(users => {
      this.store.dispatch(new actions.LoadUsers({ users }));
    });
  }

  loadUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>(`${this.apiUrl}/users`);
  }

  getUsers(): Observable<Dictionary<User>> {
    return this.store.pipe(
      select('user')
    );
  }

  getUser(id: number): Observable<User> {
    return this.getUsers().pipe(
      map(users => {
        return users.entities && users.entities[id];
      })
    );
  }

  updateUser(user: User): Observable<any> {
    return this.httpClient.patch(
      `${this.apiUrl}/users/${user.id}`,
      user
    ).pipe(
      tap(response => {
        this.store.dispatch(new actions.UpdateUser({ user: response }));
      })
    );
  }

  deleteUser(user: User): Observable<any> {
    return this.httpClient.delete(`${this.apiUrl}/users/${user.id}`).pipe(
      tap(() => {
        this.store.dispatch(new actions.DeleteUser({ id: user.id.toString() }));
      })
    );
  }

}
