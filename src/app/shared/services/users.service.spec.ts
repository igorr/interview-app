import { TestBed } from '@angular/core/testing';

import { UsersService } from './users.service';
import { Store } from '@ngrx/store';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from '../reducers';
import { USERS } from './../test/users.mock';

describe('UsersService', () => {
  let usersService: UsersService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UsersService,
        Store,
      ],
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot(reducers, { metaReducers }),
      ]
    });

    usersService = TestBed.get(UsersService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(usersService).toBeTruthy();
  });

  it('should send GET request on init/loadUsers', () => {
    const req = httpMock.expectOne(`${usersService.apiUrl}/users`);
    expect(req.request.method).toBe('GET');
  });

  it('should send PATCH request on updateUser', () => {
    usersService.updateUser(USERS[0]).subscribe();
    const req = httpMock.expectOne(`${usersService.apiUrl}/users/${USERS[0].id}`);
    req.flush('');
    expect(req.request.method).toBe('PATCH');
  });

  it('should send DELETE request on deleteUser', () => {
    usersService.deleteUser(USERS[1]).subscribe();
    const req = httpMock.expectOne(`${usersService.apiUrl}/users/${USERS[1].id}`);
    req.flush('');
    expect(req.request.method).toBe('DELETE');
  });
});
